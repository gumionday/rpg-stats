#include <iostream>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <math.h>

#include "character.h"

using namespace std;

int main(int argc, char *argv[])
{
	// init RNG
	srand(time(NULL));

	// Create player
	string name;
	cout << "Please enter a character name: ";
	cin >> name;
	Character player(name,1,15,5);
	player.PrintDetails();

	while(!player.isDead())
	{
		// Create enemy
		string eName = "Skeleton"; 
		int enemyLVL = player.GetLevel();
		int attack = 2 * pow(1.25,enemyLVL);
		int health = 5 * pow(1.35,enemyLVL);
		Character enemy(eName,enemyLVL,health,attack);
		enemy.PrintDetails();

		while(!player.isDead() && !enemy.isDead())
		{
			cout << endl;
			// player attacks enemy
			double pAttack = player.GetHitDamage();
			enemy.DamageHealth(pAttack);				
			cout << player.GetName() << " does " << pAttack << " damage to " << enemy.GetName() << endl;
			//player.PrintDetails();

			if(!enemy.isDead())
			{
				//enemy attacks player
				double eAttack = enemy.GetHitDamage();
				player.DamageHealth(eAttack);
				cout << enemy.GetName() << " does " << eAttack << " damage to " << player.GetName() << endl;
				//enemy.PrintDetails();
			}
			else if(enemy.isDead())
			{
				cout << enemy.GetName() << " is DEAD. " << endl;
				int expGained = (enemy.GetLevel()) * 2;
				player.AddExp(expGained);
			}
		}
		cout << endl;
		player.PrintDetails();
		cout << endl;
	}

	// Simulate leveling
	//player.AddExp(100);
	//player.PrintDetails();
	//player.AddExp(20);
	//player.PrintDetails();
	//player.AddExp(30);
	//player.PrintDetails();
	//player.AddExp(40);

	//// Simulate Attacking
	//
	//// Simulate being Damaged until dead
	//while(!player.isDead())
	//{
	//	player.DamageHealth(5);
	//	player.PrintDetails();
	//}

	//cout << endl;
	//cout << "Player is dead, time to revive" << endl;

	// Simulate healing
	//player.HealHealth();
	cout << endl;
	player.PrintDetails();
	cout << "Thanks for Playing. " << endl;

	system("pause");
}
