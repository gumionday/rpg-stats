#include <ctime>
#include <iostream>

using namespace std;

class Character
{
private:
	string name;
	int currentLVL;
	int currentHP;
	int currentEXP;
	double attackDamage;
	double hitDamage;
	int maxHP;
	int maxAttack;
	int Exp;
	int toNextLevel;
	double levelUpExp;
	double levelUpHp;
	double levelUpAttack;
public:
	Character(){}
	Character(string title, int currentLVL, int currentHP, int attackDamage)
	{
		name = title;
		this->currentHP = currentHP;
		currentEXP = 0;
		this->attackDamage = attackDamage;
		this->currentLVL = currentLVL;
		maxHP = currentHP;
		maxAttack = attackDamage;
		toNextLevel = 10;
		levelUpExp = 1.35;
		levelUpHp = 1.25;
		levelUpAttack = 1.30;
	}
	~Character(void);
	void AddExp(int Exp);
	void PrintDetails();
	void DamageHealth(int damage);
	void HealHealth();
	bool isDead();
	double GetHitDamage();
	int GetLevel();
	string GetName();

	friend class Player;
	friend class Enemy;
};


//function definitions
Character::~Character(void)
{}
void Character::AddExp(int Exp)
{
	this->Exp = Exp;
	currentEXP = currentEXP + Exp;
	cout << "Gained " << Exp << " EXP " << endl;
	while(currentEXP >= toNextLevel)
	{
		cout << " ******************LEVEL UP!****************** " << endl;
		currentLVL++;
		maxHP = maxHP * levelUpHp;
		maxAttack = maxAttack * levelUpAttack;
		currentEXP = currentEXP - toNextLevel;
		toNextLevel = toNextLevel * levelUpExp;
		currentHP = maxHP;
		attackDamage = maxAttack;
		PrintDetails();
        cout << endl;
	}
}
void Character::PrintDetails()
{
	cout << name << endl;
	cout << "Level: " << currentLVL << endl;
	cout << "Hit Points: " << currentHP << endl;
	cout << "Attack Damage: " << attackDamage << endl;
	cout << "EXP: " << currentEXP << endl;
	cout << "EXP needed to level up: " << toNextLevel << endl;
}
void Character::DamageHealth(int damage)
{
	currentHP = currentHP - damage;
}
void Character::HealHealth()
{
	currentHP = maxHP;
}
bool Character::isDead()
{
	if(currentHP <= 0)
	{	
		return true;
	}
	else
		return false;
}
double Character::GetHitDamage()
{
	int maxAttack = attackDamage;
	int minAttack = attackDamage * 0.8;
	int difference = maxAttack - minAttack;
	int hitDamage = (rand() % difference) + minAttack + 1;
	return hitDamage;
}
int Character::GetLevel()
{
	return currentLVL;
}
string Character::GetName()
{
	return name;
}



